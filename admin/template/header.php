<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/admin/process/check.php');


?>
<!DOCTYPE html>
<html>
<head>
    <title>PLPD dispatch - Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Modal css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Bootstrap -->
    <link href="/admin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- jquery -->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />


    <link rel="shortcut icon" type="image/png" href="../assets/dispatch.ico"/>



    <!-- icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">




    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

    <!-- Summernote -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <!-- styles -->
    <link href="/admin/assets/styles.css" rel="stylesheet">



</head>
<body>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?> /admin/index">PLPD dispatch system</a></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li><strong><h4><?php echo $randomGreeting . " " . $steamprofile['personaname'] ?></strong></h4></li>
                    <li><a href="<?php  $_SERVER['DOCUMENT_ROOT'] ?>/admin/index"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                    <li><a href="<?php  $_SERVER['DOCUMENT_ROOT'] ?>/admin/view/team"><i class="glyphicon glyphicon glyphicon-flag"></i> Team overview</a></li>
                    <li><a href="<?php  $_SERVER['DOCUMENT_ROOT'] ?>/admin/view/section"><i class="glyphicon glyphicon glyphicon-th"></i> Section</a></li>
                    <?php if($commandMember){?>
                        <li><a href="<?php  $_SERVER['DOCUMENT_ROOT'] ?>/admin/view/logs"><i class="	glyphicon glyphicon-fire"></i> Logs</a></li>
                    <?php } ?>
                    <li><?php logoutbutton(); ?></li>
                    </li>
                </ul>
            </div>
        </div>
