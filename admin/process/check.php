<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set("log_errors", 1);
ini_set("error_log", "error.log");


include('log.php');


require_once('check.php');

require($_SERVER['DOCUMENT_ROOT'].'/config/database.php');

require($_SERVER['DOCUMENT_ROOT'] . '/steamauth/steamauth.php');


$site_title = 'plpddispatch.online';

if (!isset($_SESSION['steamid'])) {


    include($_SERVER['DOCUMENT_ROOT'] ."/admin/view/login.php");
    die();


} else {


    function getSteamID64($id) {
        if (preg_match('/^STEAM_/', $id)) {
            $parts = explode(':', $id);
            return bcadd(bcadd(bcmul($parts[2], '2'), '76561197960265728'), $parts[1]);
        } elseif (is_numeric($id) && strlen($id) < 16) {
            return bcadd($id, '76561197960265728');
        } else {
            return $id; // We have no idea what this is, so just return it.
        }
    }

    //SteamID to Normal steamID
    function parseInt($string) {
        //    return intval($string);
        if(preg_match('/(\d+)/', $string, $array)) {
            return $array[1];
        } else {
            return 0;
        }
    }

    function getSteamId32($id){
        // Convert SteamID64 into SteamID

        $subid = substr($id, 4); // because calculators are fags
        $steamY = parseInt($subid);
        $steamY = $steamY - 1197960265728; //76561197960265728

        if ($steamY%2 == 1){
            $steamX = 1;
        } else {
            $steamX = 0;
        }

        $steamY = (($steamY - $steamX) / 2);
        $steamID = "STEAM_0:" . (string)$steamX . ":" . (string)$steamY;
        return $steamID;

    }

    // Check if is admin
    include ($_SERVER['DOCUMENT_ROOT'].'/steamauth/userInfo.php');



    $steamid32 = getSteamId32($_SESSION['steamid']);
    $stmt = $conn->prepare('SELECT steamid FROM admin WHERE steamid = :steamid');
    $stmt->bindParam(':steamid', $steamid32, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if( ! $row)
    {
        InsertLog($conn, "Logged in and failed (not an admin)", $steamprofile['personaname']);
        $_SESSION['error'] = 'errorLogin';
        header("Location: /index");
        die();
    }

    $steamid32 = getSteamId32($_SESSION['steamid']);
    $stmt = $conn->prepare('SELECT command FROM admin WHERE steamid = :steamid');
    $stmt->bindParam(':steamid', $steamid32, PDO::PARAM_INT);
    $stmt->execute();
    $rowAdmin = $stmt->fetch(PDO::FETCH_ASSOC);



    if( $rowAdmin['command'] == 1)
    {
        $commandMember = true;
    }else{
        $commandMember = false;
    }


    $greetings = array("Hello ", "Howdy ", "Greetings ", "Welcome ", "Hi-ya ", "What's up  ", "Hi ", "Bonjour ", "Goedendag ", "Hiya ", "All right ", "Hullo ", "Henlo ", "How-do-you-do ", "Hej ");


    shuffle($greetings);

    // First element is random now
    $randomGreeting = $greetings[0];
}?>