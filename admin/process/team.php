<?php


require_once('check.php');
//array(3) { ["rank"]=> string(23) "Probationary Dispatcher" ["team"]=> string(1) "2" ["submit"]=> string(0) "" }
if(isset($_POST['Editsubmit'])) {

    $id = $_POST['id'];
    $rank = htmlentities($_POST['rank']);
    $team = htmlentities($_POST['team']);

    $stmt = $conn->prepare('UPDATE dispatchers SET rank = :rank, team =  :team where badge_nr = :id');
    $stmt->bindParam(':rank', $rank, PDO::PARAM_STR);
    $stmt->bindParam(':team', $team, PDO::PARAM_STR);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);


    if($stmt->execute()){
        InsertLog($conn, "Edited (".$id.") succesfully Edited details:  rank:" .$rank." | Team:" .$team, $steamprofile['personaname']);
        $_SESSION['error'] = 'success';
        header("Location: ../view/team");
        die();
    }else{
        InsertLog($conn, "Failed to edit (".$id.")", $steamprofile['personaname']);
        $_SESSION['error'] = 'errorUpdate';
        header("Location: ../view/team");
        die();
    }

}

if(isset($_POST['reason'])) {


    $id = htmlentities($_POST['id']);
    $reason = htmlentities($_POST['reason']);
    $date = date("d-m-Y");
    $given_by =  $steamprofile['personaname'];
    $playtime = htmlentities($_POST['playtime']);




    $sql = 'INSERT INTO warnings (badge_nr, reason, playtime, given_by, date ) VALUES (:badgenr, :reason,:playtime, :given_by, :dates)';
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':badgenr', $id, PDO::PARAM_INT);
    $stmt->bindParam(':reason', $reason, PDO::PARAM_STR);
    $stmt->bindParam(':playtime', $playtime, PDO::PARAM_STR);
    $stmt->bindParam(':given_by', $given_by, PDO::PARAM_STR);
    $stmt->bindParam(':dates', $date, PDO::PARAM_STR);;

    if($stmt->execute()){

        InsertLog($conn, "Added a warning by (".$id.") with the reason .$reason", $steamprofile['personaname']);
        $stmtUpdate = $conn->prepare('UPDATE dispatchers SET warning_count = warning_count + 1 where badge_nr = :id');
        $stmtUpdate->bindParam(':id', $id, PDO::PARAM_INT);
        $stmtUpdate->execute();

        $_SESSION['error'] = 'Warningsuccess';
        header("Location: ../view/team");
        die();
    }else{
        InsertLog($conn, "Failed to a warning by (".$id.") with the reason .$reason", $steamprofile['personaname']);
        $_SESSION['error'] = 'oof';
        header("Location: ../view/team");
        die();
    }

}

if (isset($_POST["delete"])) {
    $id = $_POST["id"];

    $sql = 'DELETE FROM dispatchers WHERE badge_nr = :id';

    $query = $sql;

    $pdo_statement = $conn->prepare($query);

    $pdo_statement->bindValue(':id', $id, PDO::PARAM_INT);
    if ($pdo_statement->execute()) {
        InsertLog($conn, "Deleted (".$id.") succesfully", $steamprofile['personaname']);
        $_SESSION["error"] = "DeleteSuccess";
        header("Location: ../view/team");
        die();
    } else {

        InsertLog($conn, "Failed to delete (".$id.") ", $steamprofile['personaname']);
        $_SESSION['error'] = 'errorUpdate';
        header("Location: ../view/team");
        die();

    }
}


if (isset($_POST["Addsubmit"])) {

    //array(6) { ["badgenumber"]=> string(4) "1233" ["name"]=> string(12) "sdfaasdfasdf" ["warning_count"]=> string(4) "1123" ["rank"]=> string(10) "Dispatcher" ["team"]=> string(1) "1" ["Addsubmit"]=> string(0) "" }


    $id = htmlentities($_POST['badgenumber']);
    $name = htmlentities($_POST['name']);
    $warning_count = htmlentities($_POST['warning_count']);
    $rank =  htmlentities($_POST['rank']);
    $team = htmlentities($_POST['team']);



    if($id <4)
    {
        $_SESSION['error'] = 'IDtoShort';
        header("Location: ../view/team");
        die();
    }

    if(!is_numeric($team) || !is_numeric($warning_count) || !is_numeric($id)){
        $_SESSION['error'] = 'errorUpdate';
        header("Location: ../view/team");
        die();
    }

    $sql = 'INSERT INTO dispatchers (badge_nr, rank, ooc_name, team, warning_count ) VALUES (:badgenr, :rank,:ooc_name, :team, :warning_count)';
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':badgenr', $id, PDO::PARAM_INT);
    $stmt->bindParam(':rank', $rank, PDO::PARAM_STR);
    $stmt->bindParam(':ooc_name', $name, PDO::PARAM_STR);
    $stmt->bindParam(':team', $team, PDO::PARAM_INT);
    $stmt->bindParam(':warning_count', $warning_count, PDO::PARAM_INT);

    if ($stmt->execute()) {
        InsertLog($conn, "Added a new dispatcher with the following details; (".$id.") ".$rank." ".$name." in team ".$team." with the warning amount of " .$warning_count."", $steamprofile['personaname']);
        $_SESSION["error"] = "AddSuccess";
        header("Location: ../view/team");
        die();
    } else {
        $_SESSION['error'] = 'errorUpdate';
        InsertLog($conn, "Failed to  a new dispatcher with the following details (".$id.")".$rank." ".$name." in team ".$team." with the warning amount of " .$warning_count."", $steamprofile['personaname']);
        header("Location: ../view/team");
        die();

    }
}




die('no direct scripts allowed');
