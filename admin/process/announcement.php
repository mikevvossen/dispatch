<?php
require_once('check.php');

$_SESSION['error'] = false;


if (isset($_POST["editordata"])) {



    if (!isset($_POST["editordata"]) || $_POST["editordata"] == "") {
        $_SESSION['error'] = 'oof';
        header("Location: ../index.php");
        die();
    } else {


        $title = htmlentities($_POST["title"]);
        $date = date('Y-m-d H:i:s');
        $who = htmlentities($steamprofile['personaname']);

        if($_POST["public"] == "yes"){
            $public = 1;
        }else{
            $public = 0;
        }


        $template =
        "
        <p style=\"text-align: center; \"><img src=\"https://plpd.online/images/dispatch.png\" style=\"width: 225.213px; height: 225.213px;\"><br></p><p><b></b></p><p><b><br></b></p>
        ";


        $content = $template ."".$_POST["editordata"];

        $sql = 'INSERT INTO news (title, content, date, public, author) VALUES (:title, :content, :date, :public, :author)';

        $query = $sql;

        $pdo_statement = $conn->prepare($query);

        $pdo_statement->bindValue(':title', $title, PDO::PARAM_STR);
        $pdo_statement->bindValue(':content', $content, PDO::PARAM_STR);
        $pdo_statement->bindValue(':date',  $date, PDO::PARAM_STR);
        // 1 = yes , 2 = no
        $pdo_statement->bindValue(':public', $public, PDO::PARAM_INT);
        $pdo_statement->bindValue(':author', $who, PDO::PARAM_INT);




        if( $pdo_statement->execute()){
            InsertLog($conn, "Edited (".$id.") succesfully Edited a article:  Title:" .$title." | Public:" .$public, $steamprofile['personaname']);
            $_SESSION['error'] = 'success';
            header("Location: ../index");
            die();
        }else{
            InsertLog($conn, "Edited (".$id.") failed" , $steamprofile['personaname']);
            $_SESSION['error'] = 'oof';
            header("Location: ../index");
            die();
        }
    }
}
if (isset($_POST["delete"])) {
    $id = $_POST["id"];
    if (is_numeric($id)) {

        $sql = 'DELETE FROM news WHERE `id` = :id';

        $query = $sql;

        $pdo_statement = $conn->prepare($query);

        $pdo_statement->bindValue(':id', $id, PDO::PARAM_INT);
        $result = $pdo_statement->execute();
        InsertLog($conn, "Deleted article  (".$id.") ", $steamprofile['personaname']);
        $_SESSION['error'] = 'successDelete';
        header("Location: ../index");
        die();
    } else {
        InsertLog($conn, "Failed to remove article (".$id.") " , $steamprofile['personaname']);
        $_SESSION['error'] = 'oof';
        header("Location: ../index");
        die();
    }
}


if(isset($_POST['Editsubmit'])){

    $id = $_POST['id'];


    if($_POST['public'] == 'yes'){
        $_POST['public'] = 1;
    }else{
        $_POST['public'] = 0;
    }

    $title = htmlentities($_POST['title']);

    $stmt = $conn->prepare('UPDATE news SET title = :title, public =  :public where id = :id');
    $stmt->bindParam(':title', $title, PDO::PARAM_STR);
    $stmt->bindParam(':public',  $_POST['public'], PDO::PARAM_INT);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);

    if($stmt->execute()){
        InsertLog($conn, "Edited (".$id.") succesfully Edited a article:  Title:" .$title." | Public:" .$public, $steamprofile['personaname']);
        $_SESSION['error'] = 'successEdit';
        header("Location: ../index");
        die();
    }else{
        InsertLog($conn, "Edited (".$id.") failed" , $steamprofile['personaname']);
        $_SESSION['error'] = 'oof';
        header("Location: ../index");
        die();
    }
}

