<?php
require_once('check.php');



function InsertLog($conn, $action, $who){

    $date = date('Y-m-d H:i:s');
    $who2 = htmlentities($who);


    $sql = 'INSERT INTO logs (message, date, who) VALUES (:message, :date, :who)';
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':message', $action, PDO::PARAM_INT);
    $stmt->bindParam(':date', $date, PDO::PARAM_INT);
    $stmt->bindParam(':who', $who2, PDO::PARAM_INT);
    $stmt->execute();

}

function Getlog($conn){

    deleteOldLogs($conn);

    $stmt = $conn->prepare("SELECT * FROM logs ORDER BY id DESC");
    $stmt->execute();
    return $stmt->fetchAll();



}


//delete all logs what are over 30 days old
function deleteOldLogs($conn){


    $stmt = $conn->prepare(" SELECT * from logs where datediff(now(), date) > 30");
    $stmt->execute();
    $logs = $stmt->fetchAll();


   foreach ($logs as $log){

       $id = $log['id'];
       $sql = 'DELETE FROM logs WHERE id = :id';
       $query = $sql;

       $pdo_statement = $conn->prepare($query);

       $pdo_statement->bindValue(':id', $id, PDO::PARAM_INT);
       if ($pdo_statement->execute()) {
        return true;
       } else {
           InsertLog($conn, "Failed to delete all old log messsages", 'System');
           return false;
       }
   }

}