<?php
include("template/header.php");

InsertLog($conn, "Visited the home page", $steamprofile['personaname']);

$stmt = $conn->prepare("SELECT * FROM news   ORDER BY id");
$stmt->execute();
$result = $stmt->fetchAll();

?>

<script>


    $(document).ready(function () {
        $('#summernote').summernote();
        $('#table').DataTable( {
            "order": [[ 0, "desc" ]]
        } );
    });
</script>
<div class="col-md-10">

    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <?php if (isset($_SESSION['error'])) {
                        switch ($_SESSION['error']) {
                            case 'oof':
                                InsertLog($conn, "Triggered an error at the index page", $steamprofile['personaname']);
                                echo
                                "
                        <div class=\"alert alert-warning alert-dismissible\">
                        <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                        <strong>Warning!</strong> Something went wrong.
                    </div>
                    ";

                                $_SESSION['error'] = 'nothing';
                                break;
                            case 'success':
                                echo
                                "
                    <div class=\"alert alert-success alert-dismissible\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    <strong>Success!</strong> announcement added.
                </div>
                ";

                                $_SESSION['error'] = 'nothing';
                                break;
                            case 'successEdit':
                                echo
                                "
                    <div class=\"alert alert-success alert-dismissible\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    <strong>Success!</strong> announcement edited.
                </div>
                ";

                                $_SESSION['error'] = 'nothing';
                                break;

                            case 'successDelete':
                                echo
                                "
                    <div class=\"alert alert-success alert-dismissible\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    <strong>Success!</strong> announcement deleted.
                </div>
                ";

                                $_SESSION['error'] = 'nothing';
                                break;
                        }
                    }
                    ?>
                    <div class="panel-title">
                        <li class="glyphicon glyphicon-bullhorn"></li>
                        <strong> Announcements:</strong></div>

                    <div class="panel-options">
                     <?php  if($commandMember){  ?>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#modalAdd">
                            Add
                        </button>
                     <?php }?>
                    </div>
                </div>
                <div class="panel-body">


                    <table id="table" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>Written by</th>
                            <th>Title</th>
                            <th>public</th>
                            <th>Edit/Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($result as $row) {

                            if ($row['public'] == 1) {
                                $row['public'] = "Yes";
                            } else {
                                $row['public'] = "No";
                            }
                            ?>
                            <tr>
                                <td><?= $row['author'] ?></td>
                                <td><?= $row['title'] ?></td>
                                <td><?= $row['public'] ?></td>
                                <td>
                                    <form action="/admin/process/announcement.php" method="post">
                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                                data-target="#modal<?php echo $row['id'] ?>">
                                            Edit
                                        </button>

                                        <input name="id" hidden type="hidden"
                                               value="<?php echo $row['id'] ?>">
                                        <button type="submit"
                                                onclick="return confirm('Are you sure you want to delete this article?' )"
                                                name="delete" class="btn btn-default">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>

                </div>
            </div>
        </div>


        <?php include("template/footer.php"); ?>


        <?php  if($commandMember){  ?>
        <div class="modal fade" id="modalAdd" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Dispatcher</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <form method="post" action="process/announcement.php">
                                <div>
                                    <p>Title<strong>*</strong></p>
                                    <input required type="text" name="title" id="title" class="form-control">
                                </div>
                                <br>
                                <div>
                                    <p>Text:</p>
                                    <textarea id="summernote" name="editordata"></textarea>
                                </div>

                                <div>
                                    <p>Public:</p>
                                    <select name="public" id="public"
                                            type="text" class="form-control input-sm">
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                                <div>
                                    <br>
                                    <button type="submit" name="submit" id="submit" class="btn btn-primary float-right">
                                        Save
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php } ?>

        <?php foreach ($result as $row) { ?>

            <div class="modal fade" id="modal<?php echo $row['id'] ?>" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="id" data-service="<?php echo $row['id'] ?>">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">
                                    Edit: <?php echo $row['title'] . ' ( Written by ' . $row['author'] . ')' ?></h4>
                            </div>
                            <div class="modal-body">
                                    <div class="modal-body">
                                        <form action="/admin/process/announcement.php" method="post">
                                        <input name="id" hidden type="hidden" value="<?php echo $row['id'] ?>">
                                        <br> <br>
                                        <div>
                                            <p>Title<strong>*</strong></p>
                                            <input value="<?= $row['title'] ?>" required type="text" name="title"
                                                   id="title"
                                                   class="form-control">
                                        </div>
                                        <br>

                                        <div>
                                            <p>Public:</p>
                                            <select name="public" id="public"
                                                    type="text" class="form-control input-sm">
                                                <option <?php if ($row['public'] == 1) echo 'selected'; ?> value="yes">
                                                    Yes
                                                </option>
                                                <option <?php if ($row['public'] == 0) echo 'selected'; ?> value="no">No
                                                </option>
                                            </select>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" name="Editsubmit" id="Editsubmit"
                                                    class="btn btn-primary mr-auto">
                                                Save
                                            </button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                        </form>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>


