<?php
include($_SERVER['DOCUMENT_ROOT'] . "/admin/template/header.php");

InsertLog($conn, "Visited the team page", $steamprofile['personaname']);

$stmt = $conn->prepare("SELECT * FROM dispatchers ORDER BY team");
$stmt->execute();
$result = $stmt->fetchAll();


function getWarnings($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM warnings WHERE badge_nr = $id");
    $stmt->execute();
    return $stmt->fetchAll();
}


?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#employee_data').DataTable();
        $('#warn_data').DataTable();
    });
</script>


<div class="col-md-10">

    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">

                    <?php if (isset($_SESSION['error'])) {
                        switch ($_SESSION['error']) {
                            case 'errorUpdate':
                                InsertLog($conn, "Triggered an error at the team page while updating", $steamprofile['personaname']);
                                echo
                                "
                        <div class=\"alert alert-warning alert-dismissible\">
                        <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                        <strong>Warning!</strong> Something went wrong.
                    </div>
                    ";

                                $_SESSION['error'] = 'nothing';
                                break;
                            case 'success':
                                echo
                                "
                    <div class=\"alert alert-success alert-dismissible\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    <strong>Success!</strong> record  changed.
                </div>
                ";

                                $_SESSION['error'] = 'nothing';
                                break;

                            case 'Warningsuccess':
                                echo
                                "
                    <div class=\"alert alert-success alert-dismissible\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    <strong>Success!</strong> warning  added.
                </div>
                ";

                                $_SESSION['error'] = 'nothing';
                                break;


                            case 'DeleteSuccess':
                                echo
                                "
                    <div class=\"alert alert-success alert-dismissible\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    <strong>Success!</strong> record deleted.
                </div>
                ";

                                $_SESSION['error'] = 'nothing';
                                break;
                            case 'IDtoShort':
                                InsertLog($conn, "Triggered an error at the team page (ID to short)", $steamprofile['personaname']);
                                echo
                                "
                        <div class=\"alert alert-warning alert-dismissible\">
                        <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                        <strong>Warning!</strong> Given Badge number to short.
                    </div>
                    ";

                                $_SESSION['error'] = 'nothing';
                                break;
                            case 'AddSuccess':
                                echo
                                "
                    <div class=\"alert alert-success alert-dismissible\">
                    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                    <strong>Success!</strong> Record  added.
                </div>
                ";

                                $_SESSION['error'] = 'nothing';
                                break;

                        }

                    }
                    ?>
                    <div class="panel-title">
                        <li class="glyphicon glyphicon-user"></li>
                        <strong> Team Overview:</strong></div>
                    <br>

                    <div class="panel-options">
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#modalAdd">
                            Add
                        </button>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="employee_data" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Badge Nummer</th>
                                <th>Rank</th>
                                <th>Name</th>
                                <th>Team</th>
                                <th>Warning count</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($result as $row) {

                                switch ($row['team']) {
                                    case "1":
                                        $row['team'] = "Team A";
                                        break;
                                    case "2":
                                        $row['team'] = "Team B";
                                        break;
                                    case "3":
                                        $row['team'] = "Team C";
                                        break;
                                    case "4":
                                        $row['team'] = "Team D";
                                        break;
                                    case "10":
                                        $row['team'] = "Command Team";
                                        break;
                                    case "11":
                                        $row['team'] = "Senior Team";
                                        break;
                                }
                                ?>
                                <tr>
                                    <td><?php echo $row['badge_nr'] ?></td>
                                    <td><?php echo $row['rank'] ?></td>
                                    <td><?php echo $row['ooc_name'] ?></td>
                                    <td><?php echo $row['team'] ?></td>
                                    <td><?php echo $row['warning_count'] ?></td>
                                    <form action="../process/team.php" method="post">
                                        <td>
                                            <button type="button"  class="btn btn-info" data-toggle="modal"
                                                    data-target="#modalView<?php echo $row['badge_nr'] ?>">
                                                View
                                            </button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                    data-target="#modalWarn<?php echo $row['badge_nr'] ?>">
                                                Warn
                                            </button>
                                            <button type="button" class="btn btn-success" data-toggle="modal"
                                                    data-target="#modal<?php echo $row['badge_nr'] ?>">
                                                Edit
                                            </button>

                                            <input name="id" hidden type="hidden"
                                                   value="<?php echo $row['badge_nr'] ?>">
                                            <button type="submit"
                                                    onclick="return confirm('Are you sure you want to delete this row?' )"
                                                    name="delete" class="btn btn-default">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </button>

                                        </td>
                                    </form>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/admin/template/footer.php"); ?>


<?php foreach ($result as $row) {

    (isset($row['team'])) ? $team = $row['team'] : $team = 1;
    (isset($row['rank'])) ? $rank = $row['rank'] : $rank = 'Probationary Dispatcher';
    ?>

    <div class="modal fade" id="modal<?php echo $row['badge_nr'] ?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit: <?php echo $row['ooc_name'] . ' (' . $row['badge_nr'] . ')' ?></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <form method="post" action="../process/team.php">
                            <input name="id" hidden type="hidden" value="<?php echo $row['badge_nr'] ?>">
                            <div>
                                <p>Rank:</p>
                                <select name="rank" id="rank"
                                        type="number" class="form-control input-sm">
                                    <option <?php if ($rank == 'dispatcher') echo 'selected'; ?> value="Dispatcher">
                                        Dispatcher
                                    </option>
                                    <option <?php if ($rank == 'Probationary Dispatcher') echo 'selected'; ?>
                                            value="Probationary Dispatcher">Probationary Dispatcher
                                    </option>
                                </select>
                            </div>
                            <br> <br>
                            <div>
                                <p>Team:</p>
                                <select name="team" id="team"
                                        type="number" class="form-control input-sm">
                                    <option <?php if ($team == 1) echo 'selected'; ?> value="1">Team A</option>
                                    <option <?php if ($team == 2) echo 'selected'; ?> value="2">Team B</option>
                                    <option <?php if ($team == 3) echo 'selected'; ?> value="3">Team C</option>
                                    <option <?php if ($team == 4) echo 'selected'; ?> value="4">Team D</option>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <p><b>Note:</b> This is only in our database, nothing done on PLPD.online</p>
                                <button type="submit" name="Editsubmit" id="Editsubmit" class="btn btn-primary mr-auto">
                                    Save
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalWarn<?php echo $row['badge_nr'] ?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Warn: <?php echo $row['ooc_name'] . ' (' . $row['badge_nr'] . ')' ?></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <form method="post" action="../process/team.php">
                            <input name="id" hidden type="hidden" value="<?php echo $row['badge_nr'] ?>">
                            <h4><b>Note:</b> The warning will be listed for that month. For example if you add a warning
                                on the 21 december the warning will count for december</h4>
                            <hr>

                            <div>
                                <p>Playtime:</p>
                                <input type="number" class="form-control" name="playtime" id="playtime">
                            </div>

                            <div>
                                <p>Note:</p>
                                <textarea placeholder="Note: " required style="resize: vertical" name="reason"
                                          id="reason"
                                          cols="30" rows="3"
                                          class="form-control" maxlength="600"></textarea>
                            </div>

                            <div class="modal-footer">
                                <p><b>Note:</b> This is only in our database, nothing done on PLPD.online</p>
                                <button type="submit" name="submit" id="submit" class="btn btn-primary mr-auto">Save
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalView<?php echo $row['badge_nr'] ?>" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Viewing: <?php echo $row['ooc_name'] . ' (' . $row['badge_nr'] . ')' ?></h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table id="warn_data" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Given By</th>
                                    <th>Reason</th>
                                    <th>Playtime</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach (getWarnings($conn, $row['badge_nr']) as $rowWarning) { ?>
                                    <tr>
                                        <td>
                                            <?php
                                            echo $rowWarning['given_by']
                                            ?>
                                        </td>
                                        <td><?php echo $rowWarning['reason'] ?></td>
                                        <td><?php echo $rowWarning['playtime'] ?></td>
                                        <td><?php echo $rowWarning['date'] ?></td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <p><b>Note:</b> This is only in our database, nothing done on PLPD.online</p>
                        <button type="submit" name="submit" id="submit" class="btn btn-primary mr-auto">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php } ?>


<div class="modal fade" id="modalAdd" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Dispatcher</h4>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <form method="post" action="../process/team.php">
                        <div>
                            <p>Badge number:</p>
                            <input required type="number" class="form-control" name="badgenumber" id="badgenumber">
                        </div>
                        <div>
                            <p>Name:</p>
                            <input required type="text" class="form-control" name="name" id="name">
                        </div>
                        <div>
                            <p>Warning count:</p>
                            <input required type="number" class="form-control" name="warning_count" id="warning_count">
                        </div>
                        <div>
                            <p>Rank:</p>
                            <select required name="rank" id="rank"
                                    type="number" class="form-control input-sm">
                                <option value="Dispatcher">Dispatcher</option>
                                <option value="Probationary Dispatcher">Probationary Dispatcher</option>
                            </select>
                        </div>
                        <br> <br>
                        <div>
                            <p>Team:</p>
                            <select required name="team" id="team"
                                    type="number" class="form-control input-sm">
                                <option value="1">Team A</option>
                                <option value="2">Team B</option>
                                <option value="3">Team C</option>
                                <option value="4">Team D</option>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <p><b>Note:</b> This is only in our database, nothing done on PLPD.online</p>
                            <button type="submit" name="Addsubmit" id="Addsubmit" class="btn btn-primary mr-auto">Save
                            </button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
