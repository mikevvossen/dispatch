
<!DOCTYPE html>
<html>
<head>
    <title>PLPD observation System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- styles -->
    <link href="../assets/css/styles.css" rel="stylesheet">

    <style>
        .Loginwrapper {
            text-align: center;
        }

        .Loginbutton {
            appearance: none;
            -moz-appearance: none;
            -webkit-appearance: none;
            height: 30px;
            width: 200px;
            left: 50%;
            top: 50%;
            margin-top: -15px;   /* = -height / 2   */
            margin-left: -100px; /* = -width / 2    */
            position: fixed;     /* Fixed is better */
        }
        body {
            margin: 0;
            padding: 0;
            font-family: "arial", heletica, sans-serif;
            font-size: 12px;
            background: #2980b9 url('https://static.tumblr.com/03fbbc566b081016810402488936fbae/pqpk3dn/MRSmlzpj3/tumblr_static_bg3.png') repeat 0 0;
            -webkit-animation: 10s linear 0s normal none infinite animate;
            -moz-animation: 10s linear 0s normal none infinite animate;
            -ms-animation: 10s linear 0s normal none infinite animate;
            -o-animation: 10s linear 0s normal none infinite animate;
            animation: 10s linear 0s normal none infinite animate;

        }

        @-webkit-keyframes animate {
            from {
                background-position: 0 0;
            }
            to {
                background-position: 500px 0;
            }
        }

        @-moz-keyframes animate {
            from {
                background-position: 0 0;
            }
            to {
                background-position: 500px 0;
            }
        }

        @-ms-keyframes animate {
            from {
                background-position: 0 0;
            }
            to {
                background-position: 500px 0;
            }
        }

        @-o-keyframes animate {
            from {
                background-position: 0 0;
            }
            to {
                background-position: 500px 0;
            }
        }

        @keyframes animate {
            from {
                background-position: 0 0;
            }
            to {
                background-position: 500px 0;
            }
        }

    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<div class="Loginbutton">
    <?php
    if(!isset($_SESSION['steamid'])) {
       loginbutton(); //login button

    }  else {
        include ('steamauth/userInfo.php');
        //Protected content
        echo "Welcome back " . $steamprofile['personaname'] . "</br>";
        echo "here is your avatar: </br>" . '<img src="'.$steamprofile['avatarfull'].'" title="" alt="" /><br>'; // Display their avatar!

        logoutbutton();
    }
    ?>
</div>
