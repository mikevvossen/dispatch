<?php
include($_SERVER['DOCUMENT_ROOT'] . "/admin/template/header.php");

InsertLog($conn, "Visited the section page", $steamprofile['personaname']);

$stmt = $conn->prepare("SELECT * FROM dispatchers WHERE team LIKE 1 ORDER BY rank");
$stmt->execute();
$teamA = $stmt->fetchAll();

$stmt = $conn->prepare("SELECT * FROM dispatchers WHERE team LIKE 2  ORDER BY rank");
$stmt->execute();
$teamB = $stmt->fetchAll();

$stmt = $conn->prepare("SELECT * FROM dispatchers WHERE team LIKE 3  ORDER BY rank");
$stmt->execute();
$teamC = $stmt->fetchAll();

$stmt = $conn->prepare("SELECT * FROM dispatchers WHERE team LIKE 4  ORDER BY rank");
$stmt->execute();
$teamD = $stmt->fetchAll();




?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#employee_data').DataTable();
        $('#warn_data').DataTable();
    });
</script>


<div class="col-md-10">

    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">
                        <li class="glyphicon glyphicon glyphicon-th"></li>
                        <strong> Section tree:</strong></div>
                    <br>

                    <div class="panel-options">
                        <a href="" onclick="location.reload()" data-rel="reload"><i
                                    class="glyphicon glyphicon-refresh"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="employee_data" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                 <th>Team</th>
                                <th>Lead by</th>
                                <th>Total members</th>
                            </tr>


                            </thead>

                            <tbody>


                            <tr>
                                <td rowspan="1">Team A</td>
                                <td rowspan="1">Smoof</td>
                                <td style="text-align: center" rowspan="1"><?php echo count($teamA); ?></td>
                            <?php foreach ($teamA as $row) {

                                switch ($row['rank']) {
                                    case "Probationary Dispatcher":
                                       $color = "yellow;";
                                        break;
                                    case "Dispatcher":
                                        $color = "lime;";
                                        break;

                                }
                                ?>

                                    <th style=" text-align: center; background-color:<?php echo $color ?>">
                                        <div class="label label-default"
                                             style="display:inline-block; white-space: nowrap;  background-color:#3889F2;  ">
                                            <div style="color:white">
                                                <?php echo $row['ooc_name']; ?>
                                            </div>
                                        </div
                                    </th>

                            <?php } ?>
                            </tr>

                            <tr>
                                <td rowspan="1">Team B</td>
                                <td rowspan="1">Jayw</td>
                                <td style="text-align: center" rowspan="1"><?php echo count($teamB); ?></td>
                                <?php foreach ($teamB as $row) {

                                    switch ($row['rank']) {
                                        case "Probationary Dispatcher":
                                            $color = "yellow;";
                                            break;
                                        case "Dispatcher":
                                            $color = "lime;";
                                            break;

                                    }
                                    ?>

                                    <th style=" text-align: center; background-color:<?php echo $color ?>">
                                        <div class="label label-default"
                                             style="display:inline-block; white-space: nowrap;  background-color:#3889F2;  ">
                                            <div style="color:white">
                                                <?php echo $row['ooc_name']; ?>
                                            </div>
                                        </div
                                    </th>

                                <?php } ?>
                            </tr>

                            <tr>
                                <td rowspan="1">Team C</td>
                                <td rowspan="1">Gimic</td>
                                <td style="text-align: center" rowspan="1"><?php echo count($teamC); ?></td>
                                <?php foreach ($teamC as $row) {

                                    switch ($row['rank']) {
                                        case "Probationary Dispatcher":
                                            $color = "yellow;";
                                            break;
                                        case "Dispatcher":
                                            $color = "lime;";
                                            break;

                                    }
                                    ?>

                                    <th style=" text-align: center; background-color:<?php echo $color ?>">
                                        <div class="label label-default"
                                             style="display:inline-block; white-space: nowrap;  background-color:#3889F2;  ">
                                            <div style="color:white">
                                                <?php echo $row['ooc_name']; ?>
                                            </div>
                                        </div
                                    </th>

                                <?php } ?>
                            </tr>

                            <tr>
                                <td rowspan="1">Team D</td>
                                <td rowspan="1">Brikaas</td>
                                <td style="text-align: center" rowspan="1"><?php echo count($teamD); ?></td>
                                <?php foreach ($teamD as $row) {

                                    switch ($row['rank']) {
                                        case "Probationary Dispatcher":
                                            $color = "yellow;";
                                            break;
                                        case "Dispatcher":
                                            $color = "lime;";
                                            break;

                                    }
                                    ?>

                                    <th style=" text-align: center; background-color:<?php echo $color ?>">
                                        <div class="label label-default"
                                             style="display:inline-block; white-space: nowrap;  background-color:#3889F2;  ">
                                            <div style="color:white">
                                                <?php echo $row['ooc_name']; ?>
                                            </div>
                                        </div
                                    </th>

                                <?php } ?>
                            </tr>



                            </tbody>
                        </table>
                        <div style="float: left; width: 20px; height: 20px; margin: 5px; border: 1px solid rgba(0, 0, 0, .2); background: lime"><p style="margin-left: 25px;">Dispatcher</p></div><br><br>
                        <div style="float: left; width: 20px; height: 20px; margin: 5px; border: 1px solid rgba(0, 0, 0, .2); background: yellow"><p style="margin-left: 25px;">Probationary Dispatcher</p></div><br><br><br><br>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/admin/template/footer.php"); ?>




