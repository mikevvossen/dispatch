<?php
include($_SERVER['DOCUMENT_ROOT'] . "/admin/template/header.php");

if(!$commandMember){
    InsertLog($conn, "Visited the log page without being part of the command team 0:", $steamprofile['personaname']);
    die('no access to this soz');
}
$logs = Getlog($conn);

InsertLog($conn, "Visited the log page", $steamprofile['personaname']);
?>

<script type="text/javascript">

    $(document).ready(function() {
        $('#logs_data').DataTable( {
            "order": [[ 0, "desc" ]]
        } );
    } );
</script>
<div class="col-md-5">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">
                        <li class="glyphicon glyphicon-eye-open"></li>
                        <strong>Website Logs:</strong></div>
                    <br>

                    <div class="panel-options">
                        <div class="panel-options">
                            <a href="" onclick="location.reload()" data-rel="reload"><i
                                        class="glyphicon glyphicon-refresh"></i></a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="logs_data" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Message</th>
                                <th>By Who</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($logs as $row) {?>
                                <tr>
                                    <td><?php echo $row['id'] ?></td>
                                    <td><?php echo $row['date'] ?></td>
                                    <td><?php echo $row['message'] ?></td>
                                    <td><?php echo $row['who'] ?></td>
                                </tr>

                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="col-md-5">
    <div class="row">
        <div class="col-md-12">
            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">
                        <li class="glyphicon glyphicon-remove-sign"></li>
                        <strong>Error Logs:</strong></div>
                    <br>

                    <div class="panel-options">
                        <div class="panel-options">
                            <a href="" onclick="location.reload()" data-rel="reload"><i
                                        class="glyphicon glyphicon-refresh"></i></a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">

                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#home">Home</a></li>
                        <li><a data-toggle="pill" href="#menu1">Process</a></li>
                        <li><a data-toggle="pill" href="#menu2">View</a></li>
                        <li><a data-toggle="pill" href="#menu3">General</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <h3>Select a tab</h3>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <h3>Process</h3>
                            <?php
                            $file = '../process/error.log';
                            $orig = file_get_contents($file);



                            echo '<pre>';

                            echo $orig;

                            echo '</pre>';

                            ?>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <h3>View</h3>
                            <?php
                            $file = '../view/error.log';
                            $orig = file_get_contents($file);



                            echo '<pre>';

                            echo $orig;

                            echo '</pre>';

                            ?>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <h3>General</h3>
                            <?php
                            $file = '../error.log';
                            $orig = file_get_contents($file);



                            echo '<pre>';

                            echo $orig;

                            echo '</pre>';

                            ?>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/admin/template/footer.php"); ?>


