<?php
include('templates/header.php');

$stmt = $conn->prepare("SELECT * FROM news WHERE public  LIKE 1  ORDER BY id DESC");
$stmt->execute();
$result = $stmt->fetchAll();

?>
<div style="margin-top: 50px;"></div>
<?php
foreach ($result as $row) {
    ?>

    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">  <?= $row['title'] ?></h4>
                <p class="card-subtitle mb-2 text-muted"> <span class="fa fa-address-card-o"></span> <?= $row['author'] ?> | <span class="fa fa-calendar-o"></span> <?= $row['date'] ?> </i> </p>
            </div>
            <div class="card-body">
                <?= $row['content'] ?>
            </div>
        </div>
    </div>
    <br>

<?php } ?>



<?php
include('templates/footer.php');
?>




