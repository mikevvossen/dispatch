<?php
include('templates/header.php');
?>

<div style="margin-top: 40px" class="container">

    <p style="text-align: center; "><img src="https://plpd.online/images/dispatch.png"
                                         style="width: 225.213px; height: 225.213px;"><br></p>
    <p><b></b></p>
    <p><b><br></b></p>
    <h2><b><span lang="EN-US">Introduction:</span></b></h2>
    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
    <p class="MsoNormal"><span lang="EN-US">Radio Protocol is a communication precaution to ensure that the communication between Law Enforcement personal is as effectively as possible. This guide will be used to clarify and simplify standard radio procedure within the Paralake Police Department. <o:p></o:p></span>
    </p>
    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
    <h2><b><span lang="EN-US">General Radio Procedures:</span></b></h2>
    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
    <p class="MsoNormal"><b><span lang="EN-US">A </span></b><span lang="EN-US">Before transmitting it is important you make sure you are not interrupting any other radio transmission. Take a few seconds to listen if anyone is transmitting before you transmit your own message. Only interrupt other Law Enforcement Officers messages if it involves circumstances which could result in severe injuries or death.<o:p></o:p></span>
    </p>

    <p class="MsoNormal"><b><span lang="EN-US">B </span></b><span lang="EN-US">Always know what you are about to say before you start transmitting. It is important that all communication is as effective as possible. <o:p></o:p></span>
    </p>

    <p class="MsoNormal"><b><span lang="EN-US">C </span></b><span lang="EN-US">When officers are conducting a traffic stop or are conducting interviews, the officer shall monitor radio traffic. If the officer is in a unit at least one of the officers in the unit should monitor radio traffic if the other officers in the unit is busy conducting traffic stops or are conducting interviews.<o:p></o:p></span>
    </p>

    <p class="MsoNormal"><b><span lang="EN-US">D </span></b><span lang="EN-US">If Dispatch doesn't immediately answers your transmissions, wait areasonable amount of time before calling again unless itâ€™s an emergency transmission.<o:p></o:p></span>
    </p>
   
    <p class="MsoNormal"><b><span lang="EN-US">E </span></b><span lang="EN-US">The radio shall not be used for personal or inappropriate messages.</span>
    </p>
    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
    <h2><b><span lang="EN-US">When Transmitting<br></span></b><b><span lang="EN-US">&nbsp;</span></b></h2>
    <p class="MsoNormal"><span lang="EN-US">- An officer in the field will use his/her unit name prior to transmitting any information e.g. If assigned to Alpha 1 your message forwarded to dispatch should be as following: Alpha 1 to Dispatch &lt;radio message&gt; over. <o:p></o:p></span>
    </p>
    
    <p class="MsoNormal"><span lang="EN-US">- Whenever transmitting a message to an officer, refer to the unit name.<b></b></span>
    </p>
    
    <p class="MsoNormal"><span lang="EN-US">- Pronounce words slowly and distinctly and always try to compose yourself and speak with as little emotion as possible.<o:p></o:p></span>
    </p>

    <p class="MsoNormal"><span lang="EN-US">- Do not transmit a message until is clearly in mind, but don't hesitate in emergency situations.</span>
    </p>

    <p class="MsoNormal"><span lang="EN-US">-If you need to communicate and lengthy or detailed message, use the text-based radio.</span>
    </p>

    <p class="MsoNormal"><span lang="EN-US">When describing a person give information in following order:</span>
    </p>

    <p class="MsoNormal"><b><span lang="EN-US">A </span></b><span lang="EN-US">Name </span></p>
    <p class="MsoNormal"><b><span lang="EN-US">B </span></b><span lang="EN-US">Race<br>
<b>C </b>Sex</span></p>
    <p class="MsoNormal"><b><span lang="EN-US">D</span></b><span lang="EN-US"> Color of Hair</span></p>
    <p class="MsoNormal"><b><span lang="EN-US">E </span></b><span lang="EN-US">Clothing description</span>
    </p>
    <p class="MsoNormal"><b><span lang="EN-US">F </span></b><span lang="EN-US">Felony-misdemeanor or reason for broadcast</span>
    </p>

    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
    <h2><b><span lang="EN-US">Emergency<br></span></b><b><span lang="EN-US">&nbsp;</span></b></h2>

    <p class="MsoNormal"><span lang="EN-US">When an emergency exist, the officers with the emergency and the dispatchers will have first priority in use of radio until the emergency is concluded.</span>
    </p>
    <p class="MsoNormal"><span lang="EN-US">Officers not involved in an existing emergency shall not request information or make request until the emergency has been concluded.</span>
    </p>
    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span></p>
    <h2><b><span lang="EN-US">Don'ts<br></span></b><b><span lang="EN-US">&nbsp;</span></b></h2>

    <p class="MsoNormal"><b><span lang="EN-US">Don't </span></b><span lang="EN-US">use radio codes. It complicates communication and in many cases prevent successful communication. Only use the most standard codes or use them in the text-based radio</span>
    </p>
    
    <p class="MsoNormal"><b><span lang="EN-US">Don't </span></b><span lang="EN-US">explain non-relevant details </span>
    </p>

    <p class="MsoNormal"><b><span lang="EN-US">Don't</span></b><span lang="EN-US"> convey personal feelings over the radio, i.e. anger, disgust, impertinence, etc.</span>
    </p>

    <p class="MsoNormal"><span lang="EN-US"><b>Don't</b> communicate detailed messages or messages consisting of many numbers over the voice-base radio, use the text-based radio instead.</span>
    </p>


    <p>
    </p>
</div>

<?php
include('templates/footer.php');
?>




