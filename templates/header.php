<?php

include($_SERVER['DOCUMENT_ROOT'].'/config/database.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="All the information needed for the Dispatch Division">
    <meta name="author" content="Henk Maller, Fredy Standish-Newman">

    <title>PLPD Dispatch</title>


    <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/style.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="../assets/dispatch.ico"/>

    <!-- icon library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/">PLPD Dispatch</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?php if(basename($_SERVER['PHP_SELF']) == "index.php"){ echo "active"; } ?>">
                    <a class="nav-link" href="/">Home
                        <span class="sr-only"><?php if(basename($_SERVER['PHP_SELF']) == "index.php"){ echo "(current)"; } ?></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if(basename($_SERVER['PHP_SELF']) == "news.php"){ echo "active"; } ?>" href="/news">News</a>
                    <span class="sr-only"><?php if(basename($_SERVER['PHP_SELF']) == "news.php"){ echo "(current)"; } ?></span>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if(basename($_SERVER['PHP_SELF']) == "guide.php"){ echo "active"; } ?>" href="/guide">Guide</a>
                    <span class="sr-only"><?php if(basename($_SERVER['PHP_SELF']) == "guide.php"){ echo "(current)"; } ?></span>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../admin/index">Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
