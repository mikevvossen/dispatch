<?php
session_start();
include('templates/header.php');

if (isset($_SESSION['error'])) {
    switch ($_SESSION['error']) {
        case 'errorLogin':
            echo
                "

                    <br><br>
                    <div class=\"container\">
                        <div class=\"alert alert-warning alert-dismissible\">
                        <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
                        <strong>Sorry " . $_SESSION['steam_personaname'] .", </strong> You are not an admin, Please contact  dispatch command if you think this is a mistake.
                    </div>
                     </div>
                    ";

            $_SESSION['error'] = 'nothing';
            break;
    }
}
?>

<div class="container">
    <div class="iconcontainer">
        <div class="row">
            <!-- ABOUT US - DISPATCH COMMAND -->
            <!-- GOALS, VISIONS, MEMBERS -->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="iconbox">
                    <div class="iconbox-icon">
                        <span class="fa fa-group"></span>
                    </div>
                    <div class="featureinfo">
                        <h4 class="text-center">The Dispatch Staff</h4>
                        <p>
                            The Dispatch Staff consist of a many very experienced and adapted officers
                            constantly trying to shape the best possible dispatching experience through
                            a dedicated trial of progress.
                        </p>
                        <!--                            <a class="btn btn-default btn-sm" href="#" role="button">View Page »</a>-->
                    </div>
                </div>
            </div>
            <!-- WHAT ARE WE WORKING ON -->
            <!-- OUR CURRENT AND FUTURE PROTJECTS -->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="iconbox">
                    <div class="iconbox-icon">
                        <span class="fa fa-lightbulb-o"></span>
                    </div>
                    <div class="featureinfo">
                        <h4 class="text-center">What are we working on</h4>
                        <p>
                            The Dispatch Division is constantly carrying out different protjects to improve the
                            dispatching experience. If you want to follow our progress and our achievings,
                            you can follow them here.
                        </p>
                        <!--                            <a class="btn btn-default btn-sm" href="#" role="button">View Page »</a>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Radio Communication - could change it to guides when we make more - May be changing this later (We could call this recources?) -->
        <!-- Radio protocol, Radio guide, how to reports people not following radio protocol -->
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="iconbox">
                    <div class="iconbox-icon">
                        <span class="fa fa-thumbs-o-up"></span>
                    </div>
                    <div class="featureinfo">
                        <h4 class="text-center">Radio Communication</h4>
                        <!-- We should do the google docs code thingy for this site -->
                        <p>
                            The Dispatch staff has made a detailed radio protocol guide. To improve your own experience
                            while dispatching, we highly recommend reading through the guide for a better understanding
                            of how to correctly communicate using the police radio as a dispatcher or a normal officer.
                        </p>
                        <a class="btn btn-default btn-sm" href="/guide" role="button">View Page »</a>
                    </div>
                </div>
            </div>
            <!-- POLICY -->
            <!-- Our policies - We should figure out something different since we got PLPD.Online -->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="iconbox">
                    <div class="iconbox-icon">
                        <span class="fa fa-info"></span>
                    </div>
                    <div class="featureinfo">
                        <h4 class="text-center">Policy</h4>
                        <p>
                            The Dispatch division got a set of policies to make the dispatch experience more enjoyable.
                            Make sure to carefully read through all policies so you know what exactly you can do and
                            what you shouldn't do while being on duty.
                        </p>
                        <a class="btn btn-default btn-sm" target="_blank"
                           href="https://plpd.online/resources/division/resources/dispatch" role="button">View Page
                            »</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include('templates/footer.php');
?>
